int D0 = 16;
int D1 = 5;
int D2 = 4;
int D3 = 0;
int D4 = 2;
int D5 = 14;
int D6 = 12;
int D7 = 13;
int D8 = 15;

int GRN = D5;
int YLW = D6;
int RED = D7;

void setup() {
  // put your setup code here, to run once:
  pinMode(GRN, OUTPUT);
  pinMode(YLW, OUTPUT);
  pinMode(RED, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
    digitalWrite(GRN, 1);
    digitalWrite(YLW, 1);
    digitalWrite(RED, 1);
    delay(5000);
    digitalWrite(GRN, 0);
    digitalWrite(YLW, 0);
    digitalWrite(RED, 0);
    delay(5000);
}
