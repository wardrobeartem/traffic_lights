# Traffic_Lights

Traffic light project for uni

## Synopsis

ESP8266 microcontroller code that gorgeously controlls coloured LEDs

## How to run?

1. In Arduino IDE > File > Preferences > Additional Board Manager URLs include the following link:

`http://arduino.esp8266.com/stable/package_esp8266com_index.json`

This will enable support for the microcontroller. 

2. IDE > Tools > Board:... > Boards Manager install esp8266 board. 
3. IDE > Tools > Board:... > ESP8266 Boards (3.0.2) select LOLIN(WEMOS) D1 R2 & mini
4. open the traffic_light.ino file in the ide and flash it to the microcontroller using "→ (Upload)" button

## Who did this?

- Oliver
- Georgii
- Mohamad
